// Courtney Sims
// CS 4300 - Homework #6
// Ray Tracing Project
// due: 11/30/2011, 9pm

// Represents a world of spheres
class World {
  PVector vp;
  PVector theLight;
  PVector lightColor;
  float ka;
  int numSpheres;
  mySphere[] theSpheres;
  PVector bgColor1;
  PVector bgColor2;
  // how wide/high a checkered section of the background is
  float sectionSize;

  // Constructs a world with a viewer position, light position, light color,
  // ambient coefficient, list of spheres, and 2 background colors (for checkering).
  World(PVector vp, PVector theLight, PVector lightColor, float ka, mySphere[] someSpheres, PVector bgColor1, PVector bgColor2) {
    this.vp = vp;
    this.theLight = theLight;
    this.lightColor = lightColor;
    this.ka = ka;
    this.numSpheres = someSpheres.length;
    this.theSpheres = new mySphere[0];
    this.bgColor1 = bgColor1;
    this.bgColor2 = bgColor2;
    this.sectionSize = width / 10;

    for (int i = 0; i < this.numSpheres; i++) {
      this.theSpheres = (mySphere[]) append(this.theSpheres, someSpheres[i]);
    }
  }


  // Generates a checkered background, the size of each section/color specified by the
  // sectionSize variable and the 2 colors specified as PVectors by c1 and c2.
  PVector checkeredBackgroundColor(PVector pos, float sectionSize, PVector c1, PVector c2) {
    float tmpX = pos.x % (sectionSize * 2);
    float tmpY = pos.y % (sectionSize * 2);

    if (((tmpX < sectionSize) && (tmpY < sectionSize)) || ((tmpX > sectionSize) && (tmpY > sectionSize))) {
      return c1;
    }
    else {
      return c2;
    }
  }


  // Adds ambient light according to this world's ka to each of this world's spheres,
  // and corrects the color components of each sphere if any of them <0 or >1 .
  void addAmbientLight() {
    for (int i = 0; i < this.numSpheres; i++) {
      this.theSpheres[i].addAmbientLight(this.ka);
      this.theSpheres[i].fixColorComponents();
    }
  }


  // Removes the given sphere from this world's list of spheres.
  mySphere[] removeSphereFromList(mySphere sph) {
    mySphere[] tempList = new mySphere[0];

    for (int s = 0; s < this.numSpheres; s++) {
      if (!sph.isEqual(this.theSpheres[s])) {
        tempList = (mySphere[]) append(tempList, this.theSpheres[s]);
      }
    }

    return tempList;
  }


  // Given a list of spheres, finds the sphere with the smallest T value at the given point
  mySphere findSmallestTSphere(mySphere[] aList, PVector pt) {
    int len = aList.length;
    mySphere closestSphere = (mySphere) aList[0];
    float smallestT = closestSphere.findT(this.vp, pt);

    for (int x = 0; x < len; x++) {
      float tempT = aList[x].findT(this.vp, pt);
      if (tempT < smallestT) {
        smallestT = tempT;
        closestSphere = (mySphere) aList[x];
      }
    }

    return closestSphere;
  }


  // In the given list of spheres, does any sphere have a T value >0
  // given the 2 coordinates P0 and P1?
  boolean anyIntersection(mySphere[] sphereList, PVector P0, PVector P1) {
    int len = sphereList.length;
    boolean foundIntersection = false;
    int count = 0;

    while ((!foundIntersection) && (count < len)) {
      float tempT = sphereList[count].findT(P0, P1);
      if (tempT >= 0) {
        foundIntersection = true;
      }
      count++;
    }

    return foundIntersection;
  }


  // Returns a PVector representing the 3 components of the color at the given coordinate pt,
  // with the given sphere sph, and the given shadow factor (used to determine how dark a color
  // should be, e.g. if the point is in a shadow we can set the shadow factor to 0.5, and to 1
  // if the point is not in shadow).
  PVector phongHighlight(mySphere sph, PVector pt, float shadowFactor) {
    PVector ptOnSphere = sph.findIntersectionPoint(this.vp, pt);
    PVector n = sph.getN(ptOnSphere);

    PVector Lprime = new PVector(this.theLight.x - ptOnSphere.x, this.theLight.y - ptOnSphere.y, this.theLight.z - ptOnSphere.z);
    float LprimeMag = Lprime.mag();
    PVector unitL = PVector.div(Lprime, LprimeMag);
    float factorNL = PVector.dot(n, unitL);

    PVector Vprime = new PVector(this.vp.x - ptOnSphere.x, this.vp.y - ptOnSphere.y, this.vp.z - ptOnSphere.z);
    float VprimeMag = Vprime.mag();
    PVector unitV = PVector.div(Vprime, VprimeMag);

    PVector unitH = PVector.div(PVector.add(unitL, unitV), 2);
    float factorNH = PVector.dot(n, unitH);
    float NHn = pow(factorNH, sph.e);

    PVector colorPart1 = PVector.mult(sph.sColor, ka);
    PVector colorPart2 = PVector.mult(sph.sColor, shadowFactor * sph.kd * factorNL);
    PVector colorPart3 = PVector.mult(this.lightColor, NHn * sph.ks);

    PVector ColorAtPoint = PVector.add(colorPart1, PVector.add(colorPart2, colorPart3));
    ColorAtPoint = this.fixColorValues(ColorAtPoint);

    return ColorAtPoint;
  }


  // Given a vector, fixes the value of each component to be between 0 and 1.
  PVector fixColorValues(PVector pv) {
    float newR = constrain(pv.x, 0, 1);
    float newG = constrain(pv.y, 0, 1);
    float newB = constrain(pv.z, 0, 1);

    return new PVector(newR, newG, newB);
  }
  
  void transformWorld(float amt) {
    if (keyPressed) {
      if (keyCode == LEFT && key == CODED) {
        this.theLight.x = this.theLight.x - amt;
        //this.theLight = new PVector(this.theLight.x - amt, this.theLight.y, this.theLight.z);
      }
      else if (keyCode == RIGHT && key == CODED) {
        this.theLight.x = this.theLight.x + amt;
        //this.theLight = new PVector(this.theLight.x + amt, this.theLight.y, this.theLight.z);
      }
      else if (keyCode == UP && key == CODED) {
        this.theLight.y = this.theLight.y - amt;
        //this.theLight = new PVector(this.theLight.x, this.theLight.y - amt, this.theLight.z);
      }
      else if (keyCode == DOWN && key == CODED) {
        this.theLight.y = this.theLight.y + amt;
        //this.theLight = new PVector(this.theLight.x, this.theLight.y + amt, this.theLight.z);
      }
      else if (key == 'i' || key == 'I') {
        this.theLight.z = this.theLight.z - amt;
        //this.theLight = new PVector(this.theLight.x, this.theLight.y, this.theLight.z + amt);
      }
      else if (key == 'o' || key == 'O') {
        this.theLight.z = this.theLight.z + amt;
        //this.theLight = new Pvector(this.theLight.x, this.theLight.y
      }
      else if (key == 'a' || key == 'A') {
        this.vp.x = this.vp.x - amt;
      }
      else if (key == 'd' || key == 'D') {
        this.vp.x = this.vp.x + amt;
      }
      else if (key == 'w' || key == 'W') {
        this.vp.y = this.vp.y - amt;
      }
      else if (key == 's' || key == 'S') {
        this.vp.y = this.vp.y + amt;
      }
      else if (key == 'r' || key == 'R') {
        this.vp.z = this.vp.z - amt;
      }
      else if (key == 'f' || key == 'F') {
        this.vp.z = this.vp.z + amt;
      }
    }
  }


  // Draws the spheres and background of this world
  void drawSphereWorld() {
    // loop through every pixel in the window
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {

        // a vector representing the pixel we're currently looking at
        PVector ijVector = new PVector(i, j, 0);

        // the color the background will be at the current pixel
        PVector bgColor = checkeredBackgroundColor(ijVector, this.sectionSize, this.bgColor1, this.bgColor2);

        // get a list of any spheres intersected between the viewer coordinate and the current pixel
        mySphere[] intersectedSphereList = new mySphere[0];
        for (int s = 0; s < this.numSpheres; s++) {
          if (this.theSpheres[s].hasIntersection(this.vp, ijVector)) {
            intersectedSphereList = (mySphere[]) append(intersectedSphereList, this.theSpheres[s]);
          }
        }

        // FOR BACKGROUND COLOR/SHADOWS
        // if no spheres intersected between the viewer and current pixel, draw background
        if (intersectedSphereList.length <= 0) {
          boolean foundIntersection = false;
          int count = 0;

          // checks for any sphere intersections between the light and the current pixel
          while ((!foundIntersection) && count < this.numSpheres) {
            if (this.theSpheres[count].hasIntersection(this.theLight, ijVector)) {
              foundIntersection = true;
            }
            count++;
          }

          // BACKGROUND - WITH SHADOWS
          // if any sphere intersected between the light and current pixel, the pixel is in shadow.
          // draw background color at half brightness
          if (foundIntersection) {
            stroke((this.lightColor.x * bgColor.x) / 2, (this.lightColor.y * bgColor.y) / 2, this.lightColor.z * bgColor.z / 2);
          }
          // BACKGROUND - NO SHADOWS
          // if no spheres intersected between the light and current pixel, draw background
          // color at full brightness
          else {
            stroke(this.lightColor.x * bgColor.x, this.lightColor.y * bgColor.y, this.lightColor.z * bgColor.z);
          }
        }

        // FOR SPHERE COLOR/SHADOWS
        // if any sphere intersected between the viewer and current pixel, draw sphere
        else {
          // given the list of intersected spheres at the given pixel,
          // finds the sphere with the smallest T
          mySphere smallestTSphere = this.findSmallestTSphere(intersectedSphereList, ijVector);
          
          // find the point on the sphere with the smallest T
          PVector pointOnSphere = smallestTSphere.findIntersectionPoint(this.vp, ijVector);
          
          // removes the sphere with the smallest T from the list of intersected spheres
          mySphere[] tempSphereList = this.removeSphereFromList(smallestTSphere);
          int tempListLen = tempSphereList.length;

          boolean foundIntersection = false;
          int count = 0;

          // checks for any intersection between pointOnSphere and the light
          while ((!foundIntersection) && (count < tempListLen)) {
            if (tempSphereList[count].hasIntersection(pointOnSphere, this.theLight)) {
              foundIntersection = true;
            }
            count++;
          }

          // SPHERE - WITH SHADOWS
          // if any spheres intersected between pointOnSphere and the light, pixel is in shadow
          if (anyIntersection(tempSphereList, pointOnSphere, this.theLight)) {
            PVector phong = this.phongHighlight(smallestTSphere, ijVector, 0.3);
            stroke(phong.x, phong.y, phong.z);
          }

          // SPHERE - NO SHADOWS
          // if no spheres intersected between pointOnSphere and the light, pixel not in shadow
          else {
            PVector phong = this.phongHighlight(smallestTSphere, ijVector, 1);
            stroke(phong.x, phong.y, phong.z);
          }
        }

        // DRAW THE PIXEL AT THE POINT
        point(ijVector.x, ijVector.y, ijVector.z);
      }
    }
  }
}

