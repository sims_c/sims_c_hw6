// Courtney Sims
// CS 4300 - Homework #6
// Ray Tracing Project
// due: 11/30/2011, 9pm

/*
 Note: the program will read in and draw any number of spheres specified
 on multiple lines (1 sphere = 1 line).  It currently prints errors if the
 input file has any empty lines, but will still draw the correct spheres,
 and will ignore any lines that begin with # .  For each sphere, it
 will need the correct number of whitespace-separated elements (10) on
 each line, otherwise it will throw an error.  Additionally, it assumes
 that file will *always* be given in the following format, where ka is the
 ambient light constant; (vx,vy,vz) is the viewer position; (Lx,Ly,Lz) is the
 light position; LcolorR LcolorG LcolorB represent the light color;
 bg1R bg1G bg1B represents the first background color; bg2R bg2G bg2B represents the
 second background color; followed by at least one line of format (cx,cy,cz,r,R,G,B,kd,ks,e)
 representing spheres (the center x y and z coordinates, radius, R G and B
 components (from 0 to 1), diffuse coefficient, specular coefficient, and specular
 exponent).
 
 ka
 vx vy vz
 Lx Ly Lz
 LcolorR LcolorG LcolorB
 bg1R bg1G bg1B
 bg2R bg2G bg2B
 cx cy cz r R G B kd ks e
 cx cy cz r R G B kd ks e
 ...
 
*/


// Declare variables.
String loadPath;
String[] lines1;
String[] lines2;
float[][] lines3;
mySphere[] sphereList;
float ka;
PVector viewerPos;
PVector lightPos;
PVector lightColor;
World theWorld;
PVector bgColor1;
PVector bgColor2;
// the amount to move the viewpoint/light by
static final float MOVE_BY_PIXEL_AMOUNT = 15;


// Initialize variables.
void setup() {
  
  size(512, 512, P3D);
  colorMode(RGB, 1.0);
  noStroke();
  
  lines2 = new String[0];
  lines3 = new float[0][0];
  sphereList = new mySphere[0];
  viewerPos = new PVector();
  lightPos = new PVector();
  
  // Open a GUI file chooser to select the path to a file.
  loadPath = selectInput();
  
  if (loadPath == null) {
    println("No file was selected.");
  }
  
  else {
    
    // Load every line from the file at the given path, remove
    // comment lines, split by whitespace and convert to float.
    lines1 = loadStrings(loadPath);
    lines2 = removeCommentLines(lines1);
    lines3 = splitAndMakeFloat(lines2);
    
    int tempLen = lines3.length;
    
    // Length must be greater than 6. lines3[0] holds ka, lines3[1] holds the viewer position,
    // lines3[2] holds the light position, lines3[3] holds the light color,
    // lines3[4] holds thefirst background color, lines3[5] holds the second background color,
    // and lines3[6] is where the first sphere is.  We require the file to have at least one sphere.
    if (tempLen > 5) {
      // get the ka value, viewer and light positions, and the 2 background colors
      ka = lines3[0][0];
      viewerPos = new PVector(lines3[1][0], lines3[1][1], lines3[1][2]);
      lightPos = new PVector(lines3[2][0], lines3[2][1], lines3[2][2]);
      lightColor = new PVector(lines3[3][0], lines3[3][1], lines3[3][2]);
      bgColor1 = new PVector(lines3[4][0], lines3[4][1], lines3[4][2]);
      bgColor2 = new PVector(lines3[5][0], lines3[5][1], lines3[5][2]);
      
      // makes spheres from the rest of the lines in the file
      for (int i = 6; i < tempLen; i++) {
        sphereList = (mySphere[]) append(sphereList, makeSphere(lines3[i]));
      }
      
      theWorld = new World(viewerPos, lightPos, lightColor, ka, sphereList, bgColor1, bgColor2);
      
      // adds ambient light to the spheres
      theWorld.addAmbientLight();
      // draws the spheres on the screen with diffuse and specular lighting
      //theWorld.drawSphereWorld();
    }
    
    else {
      println("File requires a viewer position line, light position line, and at least one sphere line.");
    }
  }
}

void draw() {
  theWorld.drawSphereWorld();
}


// Returns a new String array containing all the lines
// from the given String array *except* the ones
// beginning with # .
String[] removeCommentLines(String[] someLines) {
  String[] newLines = new String[0];

  for (int i=0; i < someLines.length; i++) {    
    if (!someLines[i].startsWith("#")) {
      newLines = append(newLines, someLines[i]);
    }
  }

  return newLines;
}


// For each line in the given array, splits the
// elements by whitespace and converts to floats.
// Returns an array of arrays. E.g. the array
// ["1 2 3", "4 5 6"] would become [[1, 2, 3], [4, 5, 6]].
float[][] splitAndMakeFloat(String[] someLines) {
  float[][] newLines = new float[0][0];

  for (int i=0; i < someLines.length; i++) {
    newLines = (float[][]) append(newLines, float(splitTokens(someLines[i])));
  }

  return newLines;
}


// Makes spheres from a line with the following format:
// [cx cy cz r R G B kd ks e]
mySphere makeSphere(float[] aLine) {

  mySphere aSphere = new mySphere();

  // Throw an exception if the length of the line is incorrect.
  if (aLine.length != 10) {
    try {
      lengthException();
    }
    catch (Exception e) {
      println(e.getMessage());
    }
  }
  else {
    
    PVector sphereCenter = new PVector(aLine[0], aLine[1], aLine[2]);
    float sphereRad = aLine[3];
    PVector sphereColor = new PVector(aLine[4], aLine[5], aLine[6]);
    float diffuseCoefficient = aLine[7];
    float specularCoefficient = aLine[8];
    float specularExponent = aLine[9];

    aSphere = new mySphere(sphereCenter, sphereRad, sphereColor, diffuseCoefficient, specularCoefficient, specularExponent);
    aSphere.fixColorComponents();
  }

  return aSphere;
}

// Used to catch errors if an array has the incorrect length.
void lengthException() throws Exception {
  throw new Exception("Please give an array of the correct length.");
}

void keyPressed() {
    theWorld.transformWorld(MOVE_BY_PIXEL_AMOUNT);
}
