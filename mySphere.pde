// Courtney Sims
// CS 4300 - Homework #6
// Ray Tracing Project
// due: 11/30/2011, 9pm

// Represents a sphere with diffuse and specular coefficients.
class mySphere {
  PVector sCenter;
  float r;
  PVector sColor;
  float kd;
  float ks;
  float e;

  // Constructs a sphere with the given center, radius, color, diffuse
  // coefficient, specular coefficient, and specular exponent
  mySphere(PVector cen, float rad, PVector col, float diff, float spec, float specE) {
    this.sCenter = cen;
    this.r = rad;
    this.sColor = col;
    this.kd = diff;
    this.ks = spec;
    this.e = specE;
  }

  // Constructs a sphere with no specified values (used for creating a temporary sphere)
  mySphere() {
    this.sCenter = new PVector();
    this.r = 0;
    this.sColor = new PVector();
    this.kd = 0;
    this.ks = 0;
    this.e = 0;
  }


  // Is this sphere the same as the given sphere?
  boolean isEqual(mySphere sph) {
    boolean checkX = abs(this.sCenter.x - sph.sCenter.x) < 0.01;
    boolean checkY = abs(this.sCenter.y - sph.sCenter.y) < 0.01;
    boolean checkZ = abs(this.sCenter.z - sph.sCenter.z) < 0.01;
    boolean checkRad = abs(this.r - sph.r) < 0.01;
    boolean checkR = abs(this.sColor.x - sph.sColor.x) < 0.01;
    boolean checkG = abs(this.sColor.y - sph.sColor.y) < 0.01;
    boolean checkB = abs(this.sColor.z - sph.sColor.z) < 0.01;
    boolean checkKD = abs(this.kd - sph.kd) < 0.01;
    boolean checkKS = abs(this.ks - sph.ks) < 0.01;
    boolean checkE = abs(this.e - sph.e) < 0.01;

    return (checkX && checkY && checkZ && checkRad && checkR && checkG && checkB && checkKD && checkKS && checkE);
  }


  // If a color component is < 0, makes it 0.  If a color
  // component is > 1, makes it 1.
  void fixColorComponents() {
    float newR = constrain(this.sColor.x, 0, 1);
    float newG = constrain(this.sColor.y, 0, 1);
    float newB = constrain(this.sColor.z, 0, 1);

    this.sColor = new PVector(newR, newG, newB);
  }


  // Adds ambient light to this sphere according to the given ka value.
  // Fixes the color components if any of them are <0 or >1.
  void addAmbientLight(float ka) {
    this.sColor = PVector.mult(this.sColor, ka);
    this.fixColorComponents();
  }


  // Finds the A, B, and C components to the quadratic equation used to compute intersection
  // for this sphere given two coordinates P0 and P1.
  PVector findABC(PVector P0, PVector P1) {
    float dx = P1.x - P0.x;
    float dy = P1.y - P0.y;
    float dz = P1.z - P0.z;
    float cx = this.sCenter.x;
    float cy = this.sCenter.y;
    float cz = this.sCenter.z;

    float a = (dx * dx) + (dy * dy) + (dz * dz);
    float b = (2 * dx * (P0.x - cx)) + (2 * dy * (P0.y - cy)) + (2 * dz * (P0.z - cz));
    float c = (cx * cx) + (cy * cy) + (cz * cz) + (P0.x * P0.x) + (P0.y * P0.y) +
      (P0.z * P0.z) + (-2 * ((cx * P0.x) + (cy * P0.y) + (cz * P0.z))) - (this.r * this.r);

    return new PVector(a, b, c);
  }


  // Is there an intersection with this sphere between the coordinates P0 and P1?
  boolean hasIntersection(PVector P0, PVector P1) {
    PVector abc = findABC(P0, P1);
    float a = abc.x;
    float b = abc.y;
    float c = abc.z;

    float discriminant = (b * b) - (4 * a * c);

    return (discriminant >= 0);
  }


  // Finds the value of T for this sphere given two coordinates P0 and P1.
  float findT(PVector P0, PVector P1) {
    PVector abc = findABC(P0, P1);
    float a = abc.x;
    float b = abc.y;
    float c = abc.z;

    float t = (-b - sqrt((b * b) - (4 * a * c))) / (2 * a);

    return t;
  }


  // Finds the coordinates of the intersection point with this sphere
  // given two points P0 and P1.
  PVector findIntersectionPoint(PVector P0, PVector P1) {
    float dx = P1.x - P0.x;
    float dy = P1.y - P0.y;
    float dz = P1.z - P0.z;

    float t = findT(P0, P1);

    float x = P0.x + (t * dx);
    float y = P0.y + (t * dy);
    float z = P0.z + (t * dz);

    return new PVector(x, y, z);
  }


  // Computes the unit normal vector to this sphere at the given coordinate
  PVector getN(PVector pt) {
    float x = (pt.x - this.sCenter.x)/this.r;
    float y = (pt.y - this.sCenter.y)/this.r;
    float z = (pt.z - this.sCenter.z)/this.r;

    return new PVector(x, y, z);
  }
}

